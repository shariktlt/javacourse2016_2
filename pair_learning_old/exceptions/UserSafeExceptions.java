package ru.innopolis.k_sharypov.pair_learning_old.exceptions;

/**
 * Created by innopolis on 28.10.16.
 */
public class UserSafeExceptions extends Exception{
    private final Integer code;
    public UserSafeExceptions(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
