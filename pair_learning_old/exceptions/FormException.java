package ru.innopolis.k_sharypov.pair_learning_old.exceptions;

/**
 * Created by innopolis on 28.10.16.
 */
public class FormException extends UserSafeExceptions {
    public FormException(String message, Integer code) {
        super(message, code);
    }
}
