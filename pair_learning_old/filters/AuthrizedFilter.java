package ru.innopolis.k_sharypov.pair_learning_old.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by innopolis on 27.10.16.
 */
public class AuthrizedFilter implements Filter {
    private String authFromJsp;
    private FilterConfig filterConfig;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        authFromJsp = filterConfig.getInitParameter("auth_form");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String path = req.getRequestURI().substring(req.getContextPath().length());
        HttpSession session = req.getSession();
        Boolean isAuthorized = (Boolean) session.getAttribute("isAuthorized");
        if ( ( isAuthorized == null || !isAuthorized) && !path.startsWith("/assets/")  && !path.startsWith("/auth/") && !path.startsWith("/reg/")) {
            ((HttpServletResponse) servletResponse).sendRedirect(authFromJsp);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
