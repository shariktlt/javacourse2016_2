package ru.innopolis.k_sharypov.pair_learning_old.entity;

import ru.innopolis.k_sharypov.pair_learning_old.entity.db.ScheduleDB;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;

/**
 * Created by innopolis on 30.10.16.
 */
public class WebUser {
    private UserEntity userEntity;
    private ScheduleEntity scheduleEntity;

    public WebUser(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public void initUser() throws EntityException {
        this.scheduleEntity = ScheduleDB.getScheduleByUserId(userEntity.getId());
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public ScheduleEntity getScheduleEntity() {
        return scheduleEntity;
    }

    public void setScheduleEntity(ScheduleEntity scheduleEntity) {
        this.scheduleEntity = scheduleEntity;
    }
}
