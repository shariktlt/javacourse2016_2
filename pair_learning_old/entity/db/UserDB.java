package ru.innopolis.k_sharypov.pair_learning_old.entity.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.k_sharypov.pair_learning_old.entity.UserEntity;

import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;

import ru.innopolis.k_sharypov.pair_learning_old.forms.PasswordProfileForm;
import ru.innopolis.k_sharypov.pair_learning_old.forms.ProfileForm;
import ru.innopolis.k_sharypov.pair_learning_old.jdbc.DBConnectionFactory;

import java.sql.*;
import java.text.ParseException;

/**
 * Created by innopolis on 29.10.16.
 */
public class UserDB {
    private static Logger logger = LoggerFactory.getLogger(UserDB.class);

    /**
     *  Create user profile
     * @param profileForm
     * @return UserEntity
     * @throws EntityException
     */
    public static UserEntity create(ProfileForm profileForm) throws EntityException {
        UserEntity user = createEntity(profileForm);
        try {
            user.setBirth(profileForm.getBirth());
        } catch (ParseException e) {
            throw new EntityException("Формат даты должен быть ДД/ММ/ГГ", 4);
        }
        if (isExistByEmail(user.getEmail())) {
            throw new EntityException("Пользователь с таким email уже зарегистрирован", 1);
        }
        doRegister(user);
        return user;
    }

    /**
     *  Update user profile except password field
     * @param id
     * @param profileForm
     * @return UserEntity with updated fields
     * @throws EntityException
     */
    public static UserEntity update(Long id, ProfileForm profileForm) throws EntityException {
        UserEntity user = createEntity(profileForm);
        try {
            user.setBirth(profileForm.getBirth());
        } catch (ParseException e) {
            throw new EntityException("Формат даты должен быть ДД/ММ/ГГ", 4);
        }
        if (isExistByEmailExceptId(user.getEmail(), id)) {
            throw new EntityException("Пользователь с таким email уже зарегистрирован", 1);
        }
        doUpdate(id, user);
        return user;
    }


    private static boolean isExistByEmailExceptId(String email, Long id) {
        boolean res = false;
        try (Connection conn = DBConnectionFactory.getConnection();
             PreparedStatement st = conn.prepareStatement("SELECT count(1)  FROM users WHERE email = ? AND id != ?")) {
            st.setString(1, email);
            st.setLong(2, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) > 0) {
                    res = true;
                }
            }
        } catch (Exception e) {
            logger.error("Failed retrieve user by email without yourself for {}/{}", id, email);
        }
        return res;
    }

    private static void doUpdate(Long id, UserEntity user) throws EntityException {
        UserEntity res = null;
        try (
                Connection conn = DBConnectionFactory.getConnection();
                PreparedStatement statement = conn.prepareStatement("UPDATE users SET  first_name = ?, last_name = ?, email = ?, birth = ?, known = ?, learn = ? WHERE id = ?");
        ) {
            statement.setString(1, user.getFirst_name());
            statement.setString(2, user.getLast_name());
            statement.setString(3, user.getEmail());
            statement.setDate(4, user.getBirthDB());
            statement.setLong(5, user.getKnown());
            statement.setLong(6, user.getLearn());
            statement.setLong(7, id);
            user.setId(id);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new EntityException("Ошибка БД, запись не удалась", 2);
            }
        } catch (Exception e) {
            logger.error("Failed update user profile for {}", id, e);
        }
    }

    private static UserEntity createEntity(ProfileForm profileForm) {
        UserEntity user = new UserEntity();
        user.setNew(true);
        user.setFirst_name(profileForm.getFirstName());
        user.setLast_name(profileForm.getLastName());
        user.setEmail(profileForm.getEmail());
        user.setPassword(profileForm.getPassword());
        user.setKnown(profileForm.getKnown());
        user.setLearn(profileForm.getLearn());
        return user;
    }



    public static boolean isExistByEmail(String email) {
        boolean res = false;
        try (Connection conn = DBConnectionFactory.getConnection();
             PreparedStatement st = conn.prepareStatement("SELECT count(1)  FROM users WHERE email = ?")) {
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) > 0) {
                    res = true;
                }
            }
        } catch (Exception e) {
            logger.error("Failed get user by email {}", email, e);
        }
        return res;
    }


    private static void doRegister(UserEntity user) throws EntityException {
        UserEntity res = null;
        try (
                Connection conn = DBConnectionFactory.getConnection();
                PreparedStatement statement = conn.prepareStatement("INSERT INTO users (first_name, last_name, email, password, birth, known, learn) VALUES(?,?,?,?,?,?,?)",
                        Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, user.getFirst_name());
            statement.setString(2, user.getLast_name());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.setDate(5, user.getBirthDB());
            statement.setLong(6, user.getKnown());
            statement.setLong(7, user.getLearn());
            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new EntityException("Ошибка БД, запись не удалась", 2);
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getLong(1));
                } else {
                    throw new EntityException("Ошибка БД, идентификатор не получен", 3);
                }
            } catch (Exception e) {
                logger.error("Failed get new profile id", e);
            }
        } catch (Exception e) {
           logger.error("Failed register profile ", e);
        }
    }

    /**
     * Get user by login & password
     * @param email
     * @param password
     * @return UserEntity of founed user
     * @throws EntityException
     */
    public static UserEntity authorize(String email, String password) throws EntityException {
        UserEntity user = new UserEntity();
        user.setEmail(email);
        user.setPassword(password);
        try (Connection conn = DBConnectionFactory.getConnection();
             PreparedStatement st = conn.prepareStatement("SELECT id, first_name, last_name, birth, known, learn  FROM users WHERE email = ? AND password = ?")) {
            st.setString(1, user.getEmail());
            st.setString(2, user.getPassword());
            ResultSet rs = st.executeQuery();

            while(rs.next()){
                user.setId(rs.getLong("id"));
                user.setFirst_name(rs.getString("first_name"));
                user.setLast_name(rs.getString("last_name"));
                user.setBirthDB(rs.getDate("birth"));
                user.setKnown(rs.getLong("known"));
                user.setLearn(rs.getLong("learn"));
            }
        }catch (Exception e){
            logger.error("Failed found user for {}", email, e);
            user = null;
        }
        if(user.getId() == null){
            throw new EntityException("Пользователь с такими данными не найден", 404);
        }
        return user;
    }

    /**
     * Update profile password
     * @param id
     * @param passwordProfileForm
     */
    public static void updatePassword(Long id, PasswordProfileForm passwordProfileForm) {
        try (
                Connection conn = DBConnectionFactory.getConnection();
                PreparedStatement statement = conn.prepareStatement("UPDATE users SET password=? WHERE id=?");
        ) {
            statement.setString(1, passwordProfileForm.getPassword());
            statement.setLong(2, id);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new EntityException("Ошибка БД, запись не удалась", 2);
            }
        } catch (Exception e) {
            logger.error("Failed update password for {}", id, e);
        }
    }
}
