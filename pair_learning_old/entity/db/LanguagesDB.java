package ru.innopolis.k_sharypov.pair_learning_old.entity.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.k_sharypov.pair_learning_old.forms.SelectOption;
import ru.innopolis.k_sharypov.pair_learning_old.jdbc.DBConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by innopolis on 29.10.16.
 */
public class LanguagesDB {
    private static Logger logger = LoggerFactory.getLogger(LanguagesDB.class);
    private static List<SelectOption> selectOptions;

    /**
     *  Returns select options for form
     * @param forceRefresh true if need refresh cache
     * @return <code>List<SelectOption></code> list of key-value
     */
    public static List<SelectOption> getSelectOptions(boolean forceRefresh){
        if(forceRefresh || selectOptions == null){
            List<SelectOption> list = new ArrayList<>();
            try (
                    Connection conn = DBConnectionFactory.getConnection();
                    Statement statement = conn.createStatement()
            ) {
                ResultSet rs = statement.executeQuery("SELECT id, title FROM languages");
                while(rs.next()){
                    SelectOption languageEntity = new SelectOption((new Long(rs.getLong("id"))).toString(), rs.getString("title"));
                    list.add(languageEntity);
                }
                selectOptions = list;
            }catch (Exception e){
                logger.error("getselectOptionException",e);
            }
        }

        return selectOptions;
    }




}
