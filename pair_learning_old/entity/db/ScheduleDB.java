package ru.innopolis.k_sharypov.pair_learning_old.entity.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.k_sharypov.pair_learning_old.entity.ScheduleEntity;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;
import ru.innopolis.k_sharypov.pair_learning_old.forms.ScheduleForm;
import ru.innopolis.k_sharypov.pair_learning_old.jdbc.DBConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by innopolis on 29.10.16.
 */
public class ScheduleDB {
    private static Logger logger = LoggerFactory.getLogger(ScheduleDB.class);

    /**
     * Create user schedule row
     * @param scheduleForm form container of input data
     * @return ScheduleEntity object
     */
    public static ScheduleEntity insertSchedule(ScheduleForm scheduleForm){
        ScheduleEntity scheduleEntity = new ScheduleEntity(scheduleForm.getSchedule(), scheduleForm.getUserId());
        try (Connection conn = DBConnectionFactory.getConnection();
             PreparedStatement st = conn.prepareStatement("INSERT INTO schedules (user_id, w1, w2, w3, w4, w5, w6, w7) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
            st.setLong(1, scheduleEntity.getUserId());
            for (int i = 0; i < 7; i++) {
                st.setBoolean(2+i, scheduleEntity.isDay(i));
            }
            if(st.executeUpdate() == 0){
                throw new EntityException("Запись не удалась", 1);
            }

        }catch (Exception e){
            logger.error("Failed insert schedule for "+scheduleForm.getUserId(), e);
        }
        return scheduleEntity;
    }

    /**
     * Update user schedule row
     * @param scheduleForm input form data
     * @return ScheduleEntity updated
     */
    public static ScheduleEntity updateSchedule(ScheduleForm scheduleForm){
        ScheduleEntity scheduleEntity = new ScheduleEntity(scheduleForm.getSchedule(), scheduleForm.getUserId());
        try (Connection conn = DBConnectionFactory.getConnection();
             PreparedStatement st = conn.prepareStatement("UPDATE schedules SET w1=?, w2=?, w3=?, w4=?, w5=?, w6=?, w7=? WHERE user_id=?")) {
            st.setLong(8, scheduleEntity.getUserId());
            for (int i = 0; i < 7; i++) {
                st.setBoolean(1+i, scheduleEntity.isDay(i));
            }
            if(st.executeUpdate() == 0){
                logger.error("Update failed for {}", scheduleEntity.getUserId());
                throw new EntityException("Обновление не удалась", 1);
            }

        }catch (Exception e){
            logger.error("Failed update schedule for <> exception", scheduleEntity.getUserId(), e);
         }
        return scheduleEntity;
    }

    /**
     *  Get user schedule
     * @param id
     * @return ScheduleEntity
     * @throws EntityException
     */
    public static ScheduleEntity getScheduleByUserId(Long id) throws EntityException {
        ScheduleEntity scheduleEntity = null;
        try (Connection conn = DBConnectionFactory.getConnection();
             PreparedStatement st = conn.prepareStatement("SELECT w1, w2, w3, w4, w5, w6, w7 FROM schedules WHERE user_id=?")) {
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                scheduleEntity = new ScheduleEntity(id);
                for (int i = 0; i < 7; i++) {
                    scheduleEntity.setDay(i, rs.getBoolean(i+1));
                }
            }

        }catch (Exception e){
           logger.error("Fail load schedule for user {}", id, e);
        }
        if(scheduleEntity == null){
            logger.error("Missed schedule data for user {}", id);
            throw new EntityException("ОшибкаБД, агрузка расписания не удалась", 500);
        }
        return scheduleEntity;
    }
}
