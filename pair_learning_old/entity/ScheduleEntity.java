package ru.innopolis.k_sharypov.pair_learning_old.entity;

/**
 * Created by innopolis on 29.10.16.
 */
public class ScheduleEntity implements Entity {
    private Boolean[] week;
    private Long userId;
    public ScheduleEntity(Boolean[] week, Long userId) {
        this.week = week;
        this.userId = userId;
    }

    public ScheduleEntity(Long userId) {
        this.userId = userId;
        this.week = new Boolean[7];
    }

    public Boolean[] getWeek() {
        return week;
    }

    public void setWeek(Boolean[] week) {
        this.week = week;
    }

    public boolean isDay(int i) {
        return week[i];
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setDay(int i, boolean aBoolean) {
        week[i]=aBoolean;
    }
}
