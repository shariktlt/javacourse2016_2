package ru.innopolis.k_sharypov.pair_learning_old.entity;

/**
 * Created by innopolis on 29.10.16.
 */
public class LanguageEntity implements Entity {
    private Long id;
    private String title;

    public LanguageEntity(long id, String title) {
        this.id = id;
        this.title = title;
    }


    public Long getId(){
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
