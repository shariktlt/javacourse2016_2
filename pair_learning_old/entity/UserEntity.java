package ru.innopolis.k_sharypov.pair_learning_old.entity;

import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;
import ru.innopolis.k_sharypov.pair_learning_old.utils.Sha256;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by innopolis on 27.10.16.
 */
public class UserEntity implements Entity {
    private Boolean isNew = false;
    private Long id;
    private String first_name;
    private String last_name;
    private String email;
    private String password;
    private Date birth;
    private Long known;
    private Long learn;
    private SimpleDateFormat simpDate;

    {
        simpDate = new SimpleDateFormat("dd/MM/yyyy");
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = getPassWordHash(password);
    }

    public boolean checkPassword(String password) throws EntityException {
        if(this.password == null){
            throw new EntityException("Пароль пустой", 5);
        }
        return this.password.equals(getPassWordHash(password));
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get birth date
     * @return Date
     */
    public Date getBirthDB() {
        return birth;
    }

    /**
     *  Get birth date string representation
     * @return String
     */
    public String getBirth(){
        return simpDate.format(birth);
    }

    public void setBirth(String birth) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy");
        this.birth = new Date(sdf.parse(birth).getTime());
    }

    public Long getKnown() {
        return known;
    }

    public void setKnown(Long known) {
        this.known = known;
    }

    public Long getLearn() {
        return learn;
    }

    public void setLearn(Long learn) {
        this.learn = learn;
    }

    public void setBirthDB(Date date){
        this.birth = date;
    }

    //TODO Move out salt string
    public static String getPassWordHash(String str) {
        try {
            String outEncoded =  Sha256.hash256(str+"some_salt");
            return outEncoded;
        } catch (Exception e) {
            return null;
        }
    }

}
