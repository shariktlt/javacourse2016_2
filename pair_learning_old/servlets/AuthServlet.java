package ru.innopolis.k_sharypov.pair_learning_old.servlets;


import ru.innopolis.k_sharypov.pair_learning_old.exceptions.UserSafeExceptions;
import ru.innopolis.k_sharypov.pair_learning_old.forms.LoginForm;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by innopolis on 27.10.16.
 */
public class AuthServlet extends HttpServlet {
    private final String authFromJsp = "/auth.jsp";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(authFromJsp);
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding ("UTF-8");
        HttpSession session = req.getSession(true);
        try {
            LoginForm loginForm = new LoginForm(req, resp);
            loginForm.process();
        } catch (UserSafeExceptions e) {
            session.setAttribute("auth_last_error", e.getMessage());
            session.setAttribute("auth_last_error_code", e.getCode());
        }

        if (!resp.isCommitted()) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(authFromJsp);
            dispatcher.forward(req, resp);
        }
    }
}
