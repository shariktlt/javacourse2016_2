package ru.innopolis.k_sharypov.pair_learning_old.forms;

/**
 * Created by innopolis on 29.10.16.
 */
public class SelectOption {
    private String key;
    private String value;

    public SelectOption(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
