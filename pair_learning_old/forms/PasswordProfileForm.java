package ru.innopolis.k_sharypov.pair_learning_old.forms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.k_sharypov.pair_learning_old.entity.UserEntity;
import ru.innopolis.k_sharypov.pair_learning_old.entity.WebUser;
import ru.innopolis.k_sharypov.pair_learning_old.entity.db.UserDB;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.FormException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by innopolis on 30.10.16.
 */
public class PasswordProfileForm extends  FormProcessor{
    private static Logger logger = LoggerFactory.getLogger(PasswordProfileForm.class);
    private String current_password;
    private String password;
    private String password_repeat;
    private final String EMPTY = "";
    public PasswordProfileForm(HttpServletRequest req, HttpServletResponse resp) {
        super(req, resp);
    }

    @Override
    protected void processInput() {
        current_password = UserEntity.getPassWordHash(getReq().getParameter("password_change_current"));
        password =  UserEntity.getPassWordHash(getReq().getParameter("password_change"));
        password_repeat =  UserEntity.getPassWordHash(getReq().getParameter("password_change_repeat"));
    }

    @Override
    protected void validate() throws FormException {
        if(EMPTY.equals(current_password) || EMPTY.equals(password) || EMPTY.equals(password_repeat)){
            throw new FormException("Для смены пароля, необходимо заполнить все поля", 8);
        }

        if (!((WebUser) getReq().getSession(false).getAttribute("webUser")).getUserEntity().getPassword().equals(current_password)) {
            throw new FormException("Некорректный текущий пароль", 10);
         }
        if(!password.equals(password_repeat)){
            throw  new FormException("Пароли не совпадают", 11);
        }
    }

    @Override
    protected void action() throws EntityException {
        UserDB.updatePassword(((WebUser)getReq().getSession(false).getAttribute("webUser")).getUserEntity().getId(),this);
        getReq().getSession(false).setAttribute("last_message", "Пароль обновлен");
        try {
            getResp().sendRedirect("/settings/");
        } catch (IOException e) {
            logger.error("Failed send redirect header",e);
        }
    }

    public String getCurrent_password() {
        return current_password;
    }

    public void setCurrent_password(String current_password) {
        this.current_password = current_password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_repeat() {
        return password_repeat;
    }

    public void setPassword_repeat(String password_repeat) {
        this.password_repeat = password_repeat;
    }
}
