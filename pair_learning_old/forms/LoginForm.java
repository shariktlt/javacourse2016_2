package ru.innopolis.k_sharypov.pair_learning_old.forms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.k_sharypov.pair_learning_old.entity.WebUser;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;
import ru.innopolis.k_sharypov.pair_learning_old.services.Authentificator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by innopolis on 30.10.16.
 */
public class LoginForm extends FormProcessor  {
    private static Logger logger = LoggerFactory.getLogger(LoginForm.class);
    String email;
    String password;

    public LoginForm(HttpServletRequest req, HttpServletResponse resp) {
        super(req, resp);
    }

    @Override
    protected void processInput() {
        email = getReq().getParameter("email");
        password = getReq().getParameter("password");
    }

    @Override
    protected void action() throws EntityException {
        WebUser webUser = Authentificator.loginByPassword(email, password);
        Authentificator.buildSession(webUser, getReq(), getResp());
        HttpSession session = getReq().getSession(false);
        String backUrl = (String) session.getAttribute("backUrl");
        if( backUrl == null){
           backUrl = "/";
        }
        session.setAttribute("backUrl", null);
        try {
            getResp().sendRedirect(backUrl);
        } catch (IOException e) {
            logger.error("Failed send redirect header", e);
        }
    }
}
