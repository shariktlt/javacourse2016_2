package ru.innopolis.k_sharypov.pair_learning_old.forms;

import ru.innopolis.k_sharypov.pair_learning_old.exceptions.FormException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by innopolis on 29.10.16.
 */
public class RegProfileForm extends ProfileForm {
    private String passwordRepeat;

    public RegProfileForm(HttpServletRequest req, HttpServletResponse resp) {
        super(req, resp);
        setCreate(true);
    }

    @Override
    protected void processInput() {
        super.processInput();
        passwordRepeat = getReq().getParameter("password_repeat");
    }

    @Override
    protected void validate() throws FormException {
        super.validate();
        if(!getPassword().equals(passwordRepeat)){
            throw new FormException("Пароли должны совпадать", 1);
        }
    }


}
