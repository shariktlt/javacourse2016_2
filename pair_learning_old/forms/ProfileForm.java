package ru.innopolis.k_sharypov.pair_learning_old.forms;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.k_sharypov.pair_learning_old.entity.WebUser;
import ru.innopolis.k_sharypov.pair_learning_old.entity.db.UserDB;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;
import ru.innopolis.k_sharypov.pair_learning_old.entity.UserEntity;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.FormException;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.UserSafeExceptions;
import ru.innopolis.k_sharypov.pair_learning_old.services.Authentificator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handle registration form
 */
public class ProfileForm extends FormProcessor {
    private static Logger logger = LoggerFactory.getLogger(ProfileForm.class);
    private  String firstName;
    private  String lastName;
    private String email;
    private String birth;
    private long known;
    private long learn;
    private  String password;
    private boolean isCreate;
    private final String EMPTY = "";

    public ProfileForm(HttpServletRequest req, HttpServletResponse resp) {
        super(req, resp);
        isCreate = false;
    }

    @Override
    protected void processInput() {
        HttpServletRequest req = getReq();
        this.firstName = req.getParameter("first_name");
        lastName = req.getParameter("last_name");
        password = req.getParameter("password");
        email = req.getParameter("email");
        birth = req.getParameter("birth");
        known = Long.parseLong(req.getParameter("known"));
        learn = Long.parseLong(req.getParameter("learn"));
    }

    /**
     *
     * This method validate input data:
     *  1) fields not empty
     *  2)
     */
    @Override
    protected void validate() throws FormException {
        if( EMPTY.equals(firstName) || EMPTY.equals(lastName) ||
                EMPTY.equals(email) || (EMPTY.equals(password) && isCreate()) ||
                EMPTY.equals(birth) || known == 0 || learn == 0){
            throw new FormException("Необходимо заполнить все поля", 1);
        }

    }

    @Override
    protected void action() throws EntityException {
        UserEntity user;
        String redirectPath;
        if(isCreate){
            user = UserDB.create(this);
            redirectPath = "/";
        }else{
            user = UserDB.update(((WebUser)getReq().getSession().getAttribute("webUser")).getUserEntity().getId(), this);
            redirectPath = "/settings/";
        }

        ScheduleForm scheduleForm = new ScheduleForm(
                getReq(),
                getResp(),
                isCreate(),
                user.getId());
        try {
            scheduleForm.process();
            WebUser webUser = new WebUser(user);
            webUser.initUser();
            if(isCreate()){
                Authentificator.buildSession(webUser, getReq(), getResp());
            }else{
                Authentificator.refreshSession(webUser, getReq(), getResp());
            }

            try {
                if(!isCreate){
                    getReq().getSession(false).setAttribute("last_message", "Профиль обновлен");
                }
                getResp().sendRedirect(redirectPath);
            } catch (IOException e) {
                logger.error("Failed send redirect header", e);
            }
        } catch (UserSafeExceptions userSafeExceptions) {
            throw new EntityException(userSafeExceptions.getMessage(), userSafeExceptions.getCode());
        }

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean isCreate() {
        return isCreate;
    }

    public void setCreate(boolean create) {
        this.isCreate = create;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public long getKnown() {
        return known;
    }

    public void setKnown(long known) {
        this.known = known;
    }

    public long getLearn() {
        return learn;
    }

    public void setLearn(long learn) {
        this.learn = learn;
    }
}
