package ru.innopolis.k_sharypov.pair_learning_old.forms;

import ru.innopolis.k_sharypov.pair_learning_old.entity.db.ScheduleDB;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by innopolis on 29.10.16.
 */
public class ScheduleForm extends FormProcessor {
    private Boolean[] schedule;
    private boolean isNew;
    private long userId;
    ScheduleForm(HttpServletRequest req, HttpServletResponse resp, boolean isNew, long userId) {
        super(req, resp);
        this.isNew = isNew;
        this.userId = userId;
    }

    @Override
    protected void processInput() {
        schedule = new Boolean[7];
        for (int i = 1; i < 8; i++) {
            String s = getReq().getParameter("w"+i);
            schedule[i-1]= s!=null;
        }
    }

    @Override
    protected void action() throws EntityException {
        if(isNew){
            ScheduleDB.insertSchedule(this);
        }else{
            ScheduleDB.updateSchedule(this);
        }
    }

    public Boolean[] getSchedule() {
        return schedule;
    }

    public void setSchedule(Boolean[] schedule) {
        this.schedule = schedule;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
