package ru.innopolis.k_sharypov.pair_learning_old.forms;

import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.FormException;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.UserSafeExceptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FormProcessor process input forms.
 * Processing input (GET/POST) should start on item cunstructtion
 * Validate input data & do action, starts after process() triggered.
 */
public abstract class FormProcessor {
    private HttpServletRequest req;
    private HttpServletResponse resp;

    FormProcessor(HttpServletRequest req, HttpServletResponse resp){
        this.req = req;
        this.resp = resp;
        this.processInput();
    }
    public void process() throws UserSafeExceptions {
        this.validate();
        this.action();
    }

    protected  void action() throws EntityException {};

    protected  void validate() throws FormException {};

    protected  void processInput(){};

    public HttpServletRequest getReq() {
        return req;
    }

    public HttpServletResponse getResp() {
        return resp;
    }
}
