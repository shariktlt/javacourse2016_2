package ru.innopolis.k_sharypov.pair_learning_old.jdbc;


import org.apache.tomcat.jdbc.pool.DataSource;

import javax.naming.InitialContext;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by innopolis on 27.10.16.
 */

public class DBConnectionFactory {
    //private static Logger logger = LoggerFactory.getLogger(DBConnectionFactory.class);
    private static DBConnectionFactory factory = null;
    //@Resource(name="jdbc/dbsource")
    private DataSource ds;

    static {
        factory = new DBConnectionFactory();
        InitialContext initContext = null;
        try {
            Class.forName("org.postgresql.Driver");
            initContext = new InitialContext();
            factory.ds = (DataSource) initContext.lookup("java:comp/env/jdbc/dbsource");
            System.out.println("loaded source");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Connection getConnection() throws  SQLException {
        return factory.ds.getConnection();
    }

}
