package ru.innopolis.k_sharypov.pair_learning_old.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.k_sharypov.pair_learning_old.entity.UserEntity;
import ru.innopolis.k_sharypov.pair_learning_old.entity.WebUser;
import ru.innopolis.k_sharypov.pair_learning_old.entity.db.UserDB;
import ru.innopolis.k_sharypov.pair_learning_old.exceptions.EntityException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by innopolis on 30.10.16.
 */
public class Authentificator {

    public static WebUser loginByPassword(String email, String password) throws EntityException {
        UserEntity user = UserDB.authorize(email, password);
        WebUser webUser = new WebUser(user);
        webUser.initUser();
        return  webUser;
    }

    public static void buildSession(WebUser webUser, HttpServletRequest req, HttpServletResponse resp){
        HttpSession session = req.getSession(true);
        session.setAttribute("webUser", webUser);
        session.setAttribute("isAuthorized", true);
    }

    public static void refreshSession(WebUser webUser, HttpServletRequest req, HttpServletResponse resp){
        HttpSession session = req.getSession(false);
        session.setAttribute("webUser", webUser);
    }
}
