package ru.innopolis.k_sharypov.pair_learning.common.models;

/**
 * Created by innopolis on 18.11.16.
 */
public class LanguageModel {
    private Long id;
    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
