package ru.innopolis.k_sharypov.pair_learning.common.service;

import ru.innopolis.k_sharypov.pair_learning.common.models.LanguageModel;

import java.util.List;

/**
 * Created by innopolis on 18.11.16.
 */
public interface LanguageService {

    public List<LanguageModel> getLanguagesList();
}
