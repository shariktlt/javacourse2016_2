package ru.innopolis.k_sharypov.pair_learning.common.factories;

/**
 * Created by innopolis on 12.11.16.
 */
public interface MapperFactoryInterface<T> {
    T getFactory();
}