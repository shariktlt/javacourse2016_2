package ru.innopolis.k_sharypov.pair_learning.common.service;

import ru.innopolis.k_sharypov.pair_learning.common.models.UserModel;

/**
 * Created by innopolis on 12.11.16.
 */
public interface UserService {
    UserModel getByUserName(String username);
}
