package ru.innopolis.k_sharypov.pair_learning.common.models;

/**
 * Created by innopolis on 12.11.16.
 */
public enum UserProfileType {
    USER("USER"),
    ADMIN("ADMIN");
    String userProfileType;

    private UserProfileType(String userProfileType) {
        this.userProfileType = userProfileType;
    }

    /**
     * Return profile type from enum
     *
     * @return
     */
    public String getUserProfileType() {
        return userProfileType;
    }
}
