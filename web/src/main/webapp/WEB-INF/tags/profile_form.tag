<%@tag description="Profile form" pageEncoding="UTF-8"%>
<%@attribute name="title" required="true"  %>
<%@attribute name="title_action" required="true" %>
<%@attribute name="body" fragment="true" %>
<%@attribute name="is_reg" required="true" %>
<%@attribute name="action_url" required="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form"
           uri="http://www.springframework.org/tags/form" %>


<form:form modelAttribute="profileForm" method="POST" action="${action_url}" class="profile-form">
    <div class="row">
        <h2>${title}</h2>
    </div>
    <div class="row">
        <label for="first_name">Имя</label>
        <form:input path="firstName" id="first_name" cssclass="ui-full-width"/>
    </div>
    <div class="row">
        <label for="last_name">Фамилия</label>
        <form:input path="lastName" id="last_name" cssclass="ui-full-width"/>
    </div>
    <div class="row">
        <label for="birth">Дата рождения</label>
        <form:input path="birth" id="birth" cssclass="ui-full-width" type="date"/>
    </div>
    <div class="row">
            <%--<label for="known">Известный язык</label>
            <t:form_select id="known" name="known" classnames="" items="${languagesList}" selected="${is_reg?param.known:sessionScope.webUser.userEntity.known}" prop_key="id" prop_value="title" default_key="0" default_value="Выберите язык"/> --%>
    </div>
    <div class="row">
        <label for="learn">Изучаемый язык</label>
        <form:select id="learn" path="learn" itemValue="id" itemLabel="title" items="${languagesList}"/>
    </div>
    <div class="row">
        <label>График обучения</label>
    </div>
    <div class="row">
        <div class="schedule">
            <label for="w1">Пн.</label>
            <form:checkbox id="w1" path="schedule.w1"/>
            <label for="w2">Вт.</label>
            <form:checkbox id="w2" path="schedule.w2"/>
            <label for="w3">Ср.</label>
            <form:checkbox id="w3" path="schedule.w3"/>
            <label for="w4">Чт.</label>
            <form:checkbox id="w4" path="schedule.w4"/>
            <label for="w5">Пт.</label>
            <form:checkbox id="w5" path="schedule.w5"/>
            <label for="w6">Сб.</label>
            <form:checkbox id="w6" path="schedule.w6"/>
            <label for="w7">Вс.</label>
            <form:checkbox id="w7" path="schedule.w7"/>
        </div>
    </div>
    <div class="row">
        <label for="login">Email</label>
        <form:input path="email" id="login" cssClass="ui-full-width" type="email"/>
    </div>
    <c:if test="${is_reg}">
    <div class="row">
        <label for="password">Пароль</label>
        <form:password path="password" cssClass="ui-full-width" id="password"/>
    </div>
    <div class="row">
        <label for="password_repeat">Повторить пароль</label>
        <form:password path="passwordRepeat" cssClass="ui-full-width" id="password_repeat"/>
    </div>
    </c:if>
    <div class="row">
        <input type="submit" class="button-primary input" value="${title_action}">
        <c:if test="${is_reg}"><a href="/auth/" class="button">Войти</a></c:if>
    </div>
</form:form>
<c:if test="${not is_reg}">
    <form method="POST" action="${action_url}">
        <div class="row">
            <h3>Обновление пароля</h3>
        </div>
        <div class="row">
            <label for="password_change_current">Текущий ароль</label>
            <input id="password_change_current" class="ui-full-width" type="password" name="password_change_current" placeholder="Текущий пароль">
        </div>
    <div class="row">
        <label for="password_change">Пароль</label>
        <input id="password_change" class="ui-full-width" type="password" name="password_change" placeholder="Пароль">
    </div>
    <div class="row">
        <label for="password_repeat">Повторить пароль</label>
        <input id="password_change_repeat" class="ui-full-width" type="password" name="password_change_repeat" placeholder="Повторить пароль">
    </div>

        <div class="row">
            <input type="submit" class="button-primary input" value="Обновить пароль" >
        </div>
    </form>
</c:if>