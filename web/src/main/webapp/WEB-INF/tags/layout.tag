<%@tag description="Layout wrapper" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="title"%>
<%@attribute name="body" fragment="true" %>
<%@attribute name="showMenu" required="true" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>${title}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/skeleton.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <link rel="icon" type="image/png" href="/images/favicon.png">
</head>
<body>
<div class="container">
    <div class="row logo">
        <h1>Парное изучение языка</h1>
    </div>
    <c:if test="${showMenu == 'true'}">
        <div class="row">
            <div class="wu">
                <div class="wu name">
                    Вы вошли как: <a href="/settings/"><c:out value="${sessionScope.webUser.userEntity.first_name} ${sessionScope.webUser.userEntity.last_name}"/><a/>
                </div>
                <div class="wu actions">
                    <a href="/logout/">Выход</a>
                </div>
            </div>
        </div>
    </c:if>
    <div class="row">
       <jsp:invoke fragment="body"/>
    </div>
</div>

</body>
</html>

