<%@tag description="Form select element with selectOption list" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="id" required="true" %>
<%@attribute name="name" required="true" %>
<%@attribute name="classnames" required="true" %>
<%@attribute name="items" required="true" type="java.util.List" %>
<%@attribute name="selected" required="true" %>
<%@attribute name="default_key" required="true" %>
<%@attribute name="default_value" required="true" %>
<%@attribute name="prop_key" %>
<%@attribute name="prop_value" %>
<c:set property="prop_id" value="${prop_id!=null?prop_id:'id'}"/>
<c:set property="prop_key" value="${prop_key!=null?prop_key:'key'}"/>
<select id="${id}" class="${classnames}" name="${name}">
    <option value="${default_key}">${default_value}</option>
    <c:forEach items="${items}" var="item">
        <option value="${item[prop_key]}"
                <c:if test="${item[prop_key] == selected}">selected="selected"</c:if>>${item[prop_value]}</option>
    </c:forEach>
</select>
