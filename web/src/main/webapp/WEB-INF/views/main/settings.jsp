<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:layout title="Настройки" showMenu="true">
    <jsp:attribute name="body">
      <div class="row centered">
          <t:profile_form title="Настройки" title_action="Сохранить" is_reg="false" action_url="/settings/" />
      </div>
  </jsp:attribute>
</t:layout>