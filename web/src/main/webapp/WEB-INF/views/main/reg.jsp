<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:layout title="Регистрация" showMenu="false">
    <jsp:attribute name="body">
      <div class="row centered">
          <t:profile_form title="Регистрация" title_action="Регистрация" is_reg="true" action_url="/reg/"/>
      </div>
  </jsp:attribute>
</t:layout>