<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:layout showMenu="false">
    <jsp:attribute name="title">Вход</jsp:attribute>
    <jsp:attribute name="body">
      <div class="row centered">
          <form method="POST" action="/auth">
              <div class="row">
                  <h2>Вход</h2>
              </div>
              <c:if test="${not empty sessionScope.auth_last_error}">
                    <div class="row form-error-message">
                        <c:out value="${sessionScope.auth_last_error}"/>
                        <c:remove var="auth_last_error" scope="session"/>
                    </div>
              </c:if>
              <div class="row">
                  <label for="email">Email</label>
                  <input id="email" class="ui-full-width" type="email" name="email" placeholder="Email">
              </div>
              <div class="row">
                  <label for="password">Password</label>
                  <input id="password" class="ui-full-width" type="password" name="password" placeholder="Password">
              </div>
              <div class="row">
                  <input type="submit" class="button-primary input" value="Войти">
                  <a href="/reg/" class="button">Регистрация</a>
              </div>
          </form>
      </div>
  </jsp:attribute>
</t:layout>