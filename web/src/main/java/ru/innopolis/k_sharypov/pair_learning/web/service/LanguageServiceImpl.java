package ru.innopolis.k_sharypov.pair_learning.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.innopolis.k_sharypov.pair_learning.common.models.LanguageModel;
import ru.innopolis.k_sharypov.pair_learning.common.service.LanguageService;
import ru.innopolis.k_sharypov.pair_learning.web.entitys.LanguageEntity;
import ru.innopolis.k_sharypov.pair_learning.web.mapper.LanguageMapper;
import ru.innopolis.k_sharypov.pair_learning.web.repositories.LanguageRepository;
import ru.innopolis.k_sharypov.pair_learning.web.repositories.UserRepository;

import java.util.List;

/**
 * Created by innopolis on 18.11.16.
 */
@Service
@Qualifier("languageService")
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    LanguageMapper mapper;

    @Autowired
    LanguageRepository languageRepository;

    @Override
    public List<LanguageModel> getLanguagesList() {
        Iterable<LanguageEntity> list = languageRepository.findAll();
        return mapper.mapList(list);
    }
}
