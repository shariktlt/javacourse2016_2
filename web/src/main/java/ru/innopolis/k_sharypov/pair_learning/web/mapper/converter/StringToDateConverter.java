package ru.innopolis.k_sharypov.pair_learning.web.mapper.converter;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by innopolis on 12.11.16.
 */
@Component
@Qualifier("dateConverter")
public class StringToDateConverter extends BidirectionalConverter<String, Date> {
    @Override
    public Date convertTo(String source, Type<Date> destinationType) {
        Date d = null;
        try {
            d = new Date(new SimpleDateFormat("dd-MM-yyyy").parse(source).getTime());
        } catch (Exception e) {

        }
        return d;
    }

    @Override
    public String convertFrom(Date source, Type<String> destinationType) {
        return new SimpleDateFormat("dd-MM-yyyy").format(source);
    }
}
