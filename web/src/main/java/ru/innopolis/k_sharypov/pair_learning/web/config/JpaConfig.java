package ru.innopolis.k_sharypov.pair_learning.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Map;
import java.util.Properties;

/**
 * Created by innopolis on 09.11.16.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("ru.innopolis.k_sharypov.pair_learning")
public class JpaConfig {
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        //em.setDataSource(dataSource());
        em.setPackagesToScan(new String[]{"ru.innopolis.k_sharypov.pair_learning"});
        em.setPersistenceUnitName("org.hibernate.mvc.jpa");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }


    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties additionalProperties() {
        Map<String, String> env = System.getenv();
        Properties properties = new Properties();
        /* general  */
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        /* connection */
        properties.setProperty("hibernate.connection_driver", "true");
        properties.setProperty("hibernate.connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
        properties.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        properties.setProperty("hibernate.connection.url", env.get("DATABASE_URL"));
      /*  properties.setProperty("hibernate.connection.username", "innopolis");
        properties.setProperty("hibernate.connection.password", "innopolis");*/
        /* pool setup  */
        properties.setProperty("hibernate.c3p0.max_size", "5");
        properties.setProperty("hibernate.c3p0.min_size", "0");
        properties.setProperty("hibernate.c3p0.acquire_increment", "1");
        properties.setProperty("hibernate.c3p0.acquire_increment", "1");
        properties.setProperty("hibernate.c3p0.idle_test_period", "300");
        properties.setProperty("hibernate.c3p0.max_statements", "0");
        properties.setProperty("hibernate.c3p0.timeout", "10");

        return properties;
    }
}
