package ru.innopolis.k_sharypov.pair_learning.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

/**
 * Created by innopolis on 06.11.16.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("customUserDetailsService")
    UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/assets/**")
                .anonymous()
                .and()
                .authorizeRequests()
                .antMatchers("/")
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/auth")
                .usernameParameter("email")
                .passwordParameter("password")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/auth", "/reg/").permitAll()
                .anyRequest().authenticated();

        http.csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/assets/*");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, @Autowired StandardPasswordEncoder standardPasswordEncoder) throws Exception {
       /* auth
                .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER");
        auth.
                inMemoryAuthentication()
                .withUser("admin").password("admin").roles("ADMIN");*/
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(standardPasswordEncoder);


    }

    @Bean
    public StandardPasswordEncoder PasswordEncoder(@Autowired String secretPasswordString) {
        return new StandardPasswordEncoder(secretPasswordString);
    }


    @Bean
    public String secretPasswordString() {
        return "testSecretString";
    }
}
