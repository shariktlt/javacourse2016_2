package ru.innopolis.k_sharypov.pair_learning.web.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Created by innopolis on 18.11.16.
 */
public class LoginFormModel {
    @NotNull
    @Email
    private String email;

    @NotNull
    @Length(min = 4)
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
