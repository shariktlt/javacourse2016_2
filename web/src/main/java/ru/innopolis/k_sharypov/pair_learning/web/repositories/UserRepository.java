package ru.innopolis.k_sharypov.pair_learning.web.repositories;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.k_sharypov.pair_learning.web.entitys.UserEntity;

/**
 * Created by innopolis on 12.11.16.
 */
@Repository
@Qualifier("userRepository")
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity getByEmail(String username);
}
