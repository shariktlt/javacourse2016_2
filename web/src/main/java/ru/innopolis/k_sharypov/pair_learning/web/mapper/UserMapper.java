package ru.innopolis.k_sharypov.pair_learning.web.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.k_sharypov.pair_learning.common.models.UserModel;
import ru.innopolis.k_sharypov.pair_learning.web.entitys.UserEntity;
import ru.innopolis.k_sharypov.pair_learning.web.fabric.MapperFactoryBean;

/**
 * Created by innopolis on 12.11.16.
 */
@Component
public class UserMapper {
    private MapperFacade mf;

    /**
     * Configure mapper for User
     *
     * @param mapperFactory
     */
    @Autowired
    public void setMapper(MapperFactoryBean mapperFactory) {
        MapperFactory mf = mapperFactory.getFactory();
        mf.classMap(UserEntity.class, UserModel.class)
                .byDefault()
                .register();
        mf.classMap(UserModel.class, UserEntity.class)
                .byDefault()
                .register();
        this.mf = mf.getMapperFacade();
    }

    /**
     * Map entity to model
     *
     * @param entity
     * @return
     */
    public UserModel map(UserEntity entity) {
        return mf.map(entity, UserModel.class);
    }

    /**
     * Map model to entity
     *
     * @param model
     * @return
     */
    public UserEntity map(UserModel model) {
        return mf.map(model, UserEntity.class);
    }
}
