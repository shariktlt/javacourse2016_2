package ru.innopolis.k_sharypov.pair_learning.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by innopolis on 06.11.16.
 */
public class SecurityInit extends AbstractSecurityWebApplicationInitializer {
}
