package ru.innopolis.k_sharypov.pair_learning.web.repositories;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.k_sharypov.pair_learning.web.entitys.LanguageEntity;

/**
 * Created by innopolis on 18.11.16.
 */
@Repository
@Qualifier("languageRepository")
public interface LanguageRepository extends CrudRepository<LanguageEntity, Long> {
    @Override
    Iterable<LanguageEntity> findAll();
}
