package ru.innopolis.k_sharypov.pair_learning.web.model;

/**
 * Created by innopolis on 18.11.16.
 */
public class ScheduleFormModel {
    private Long id;
    private Boolean w1;
    private Boolean w2;
    private Boolean w3;
    private Boolean w4;
    private Boolean w5;
    private Boolean w6;
    private Boolean w7;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getW1() {
        return w1;
    }

    public void setW1(Boolean w1) {
        this.w1 = w1;
    }

    public Boolean getW2() {
        return w2;
    }

    public void setW2(Boolean w2) {
        this.w2 = w2;
    }

    public Boolean getW3() {
        return w3;
    }

    public void setW3(Boolean w3) {
        this.w3 = w3;
    }

    public Boolean getW4() {
        return w4;
    }

    public void setW4(Boolean w4) {
        this.w4 = w4;
    }

    public Boolean getW5() {
        return w5;
    }

    public void setW5(Boolean w5) {
        this.w5 = w5;
    }

    public Boolean getW6() {
        return w6;
    }

    public void setW6(Boolean w6) {
        this.w6 = w6;
    }

    public Boolean getW7() {
        return w7;
    }

    public void setW7(Boolean w7) {
        this.w7 = w7;
    }
}
