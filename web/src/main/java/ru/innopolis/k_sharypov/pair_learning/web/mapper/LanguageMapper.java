package ru.innopolis.k_sharypov.pair_learning.web.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.k_sharypov.pair_learning.common.models.LanguageModel;
import ru.innopolis.k_sharypov.pair_learning.web.entitys.LanguageEntity;
import ru.innopolis.k_sharypov.pair_learning.web.fabric.MapperFactoryBean;

import java.util.List;

/**
 * Created by innopolis on 18.11.16.
 */
@Component
public class LanguageMapper {
    private MapperFacade mf;

    /**
     * Configure mapper for User
     *
     * @param mapperFactory
     */
    @Autowired
    public void setMapper(MapperFactoryBean mapperFactory) {
        MapperFactory mf = mapperFactory.getFactory();
        mf.classMap(LanguageEntity.class, LanguageModel.class)
                .byDefault()
                .register();
        mf.classMap(LanguageModel.class, LanguageEntity.class)
                .byDefault()
                .register();
        this.mf = mf.getMapperFacade();
    }

    /**
     * Map entity to model
     *
     * @param entity
     * @return
     */
    public LanguageModel map(LanguageEntity entity) {
        return mf.map(entity, LanguageModel.class);
    }

    /**
     * Map model to entity
     *
     * @param model
     * @return
     */
    public LanguageEntity map(LanguageModel model) {
        return mf.map(model, LanguageEntity.class);
    }

    /**
     * Map entitys to model list
     *
     * @param entity
     * @return
     */
    public List<LanguageModel> mapList(Iterable<LanguageEntity> entity) {
        return mf.mapAsList(entity, LanguageModel.class);
    }


}
