package ru.innopolis.k_sharypov.pair_learning.web.model;

/**
 * Created by innopolis on 18.11.16.
 */
public class ProfileFormModel {
    private Long id;
    private String firstName;
    private String lastName;
    private String birth;
    private String email;
    private String password;
    private String passwordRepeat;
    private Long learn;
    private Long known;
    private ScheduleFormModel schedule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    public Long getLearn() {
        return learn;
    }

    public void setLearn(Long learn) {
        this.learn = learn;
    }

    public Long getKnown() {
        return known;
    }

    public void setKnown(Long known) {
        this.known = known;
    }

    public ScheduleFormModel getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleFormModel schedule) {
        this.schedule = schedule;
    }
}

