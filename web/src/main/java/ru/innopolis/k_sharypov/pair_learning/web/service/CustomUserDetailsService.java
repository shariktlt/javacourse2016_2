package ru.innopolis.k_sharypov.pair_learning.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.innopolis.k_sharypov.pair_learning.common.models.UserModel;
import ru.innopolis.k_sharypov.pair_learning.common.service.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by innopolis on 12.11.16.
 */
@Service
@Qualifier("сustomUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel user = userService.getByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        return new User(username, user.getPassword(), getAuhtority(user));
    }

    private Collection<? extends GrantedAuthority> getAuhtority(UserModel user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getType()));
        return authorities;
    }
}
