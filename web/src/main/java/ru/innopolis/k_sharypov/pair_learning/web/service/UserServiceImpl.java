package ru.innopolis.k_sharypov.pair_learning.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.k_sharypov.pair_learning.common.models.UserModel;
import ru.innopolis.k_sharypov.pair_learning.common.service.UserService;
import ru.innopolis.k_sharypov.pair_learning.web.entitys.UserEntity;
import ru.innopolis.k_sharypov.pair_learning.web.mapper.UserMapper;
import ru.innopolis.k_sharypov.pair_learning.web.repositories.UserRepository;

/**
 * Created by innopolis on 12.11.16.
 */
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserMapper mapper;

    @Override
    public UserModel getByUserName(String username) {
        UserEntity userEntity = userRepository.getByEmail(username);
        return mapper.map(userEntity);
    }


}
