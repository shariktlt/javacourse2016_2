package ru.innopolis.k_sharypov.pair_learning.web.fabric;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.innopolis.k_sharypov.pair_learning.common.factories.MapperFactoryInterface;

/**
 * Created by innopolis on 12.11.16.
 */
@Component
public class MapperFactoryBean implements MapperFactoryInterface {
    MapperFactory mf;

    {
        mf = new DefaultMapperFactory.Builder().build();
    }

    /**
     * Return MapperFactory
     *
     * @return
     */
    public MapperFactory getFactory() {
        return mf;
    }

    @Autowired
    public void registerDateConvertor(@Autowired BidirectionalConverter dateConverter) {
        mf.getConverterFactory().registerConverter(dateConverter);
    }
}

