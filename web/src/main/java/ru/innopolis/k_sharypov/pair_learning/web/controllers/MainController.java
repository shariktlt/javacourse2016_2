package ru.innopolis.k_sharypov.pair_learning.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.innopolis.k_sharypov.pair_learning.common.service.LanguageService;
import ru.innopolis.k_sharypov.pair_learning.web.model.LoginFormModel;
import ru.innopolis.k_sharypov.pair_learning.web.model.ProfileFormModel;
import ru.innopolis.k_sharypov.pair_learning.web.model.ScheduleFormModel;

/**
 * Created by innopolis on 10.11.16.
 */
@Controller
public class MainController {
    @Autowired
    LanguageService languageService;

    @RequestMapping("/")
    public String main() {
        return "main/index";
    }

    @RequestMapping("/auth")
    public String auth() {
        return "main/auth";
    }

    @GetMapping("/reg")
    public String reg(ModelMap modelMap) {
        ProfileFormModel formModel = new ProfileFormModel();
        formModel.setSchedule(new ScheduleFormModel());

        modelMap.addAttribute("languagesList", languageService.getLanguagesList());
        modelMap.addAttribute("is_reg", true);
        modelMap.addAttribute("profileForm", formModel);
        return "main/reg";
    }

    @PostMapping("/reg")
    public String doReg(ModelMap modelMap, LoginFormModel loginFormModel) {

        return "main/reg";
    }
}
