package ru.innopolis.k_sharypov.pair_learning.web.entitys;

import javax.persistence.*;

/**
 * Created by innopolis on 18.11.16.
 */
@Entity
@Table(name = "schedules", schema = "public", catalog = "practice2")
public class SchedulesEntity {
    @Id
    @Column(name = "user_id")
    private Long id;

    private Boolean w1;
    private Boolean w2;
    private Boolean w3;
    private Boolean w4;
    private Boolean w5;
    private Boolean w6;
    private Boolean w7;

    @Basic
    @Column(name = "w1", nullable = true)
    public Boolean getW1() {
        return w1;
    }

    public void setW1(Boolean w1) {
        this.w1 = w1;
    }

    @Basic
    @Column(name = "w2", nullable = true)
    public Boolean getW2() {
        return w2;
    }

    public void setW2(Boolean w2) {
        this.w2 = w2;
    }

    @Basic
    @Column(name = "w3", nullable = true)
    public Boolean getW3() {
        return w3;
    }

    public void setW3(Boolean w3) {
        this.w3 = w3;
    }

    @Basic
    @Column(name = "w4", nullable = true)
    public Boolean getW4() {
        return w4;
    }

    public void setW4(Boolean w4) {
        this.w4 = w4;
    }

    @Basic
    @Column(name = "w5", nullable = true)
    public Boolean getW5() {
        return w5;
    }

    public void setW5(Boolean w5) {
        this.w5 = w5;
    }

    @Basic
    @Column(name = "w6", nullable = true)
    public Boolean getW6() {
        return w6;
    }

    public void setW6(Boolean w6) {
        this.w6 = w6;
    }

    @Basic
    @Column(name = "w7", nullable = true)
    public Boolean getW7() {
        return w7;
    }

    public void setW7(Boolean w7) {
        this.w7 = w7;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SchedulesEntity that = (SchedulesEntity) o;

        if (w1 != null ? !w1.equals(that.w1) : that.w1 != null) return false;
        if (w2 != null ? !w2.equals(that.w2) : that.w2 != null) return false;
        if (w3 != null ? !w3.equals(that.w3) : that.w3 != null) return false;
        if (w4 != null ? !w4.equals(that.w4) : that.w4 != null) return false;
        if (w5 != null ? !w5.equals(that.w5) : that.w5 != null) return false;
        if (w6 != null ? !w6.equals(that.w6) : that.w6 != null) return false;
        if (w7 != null ? !w7.equals(that.w7) : that.w7 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = w1 != null ? w1.hashCode() : 0;
        result = 31 * result + (w2 != null ? w2.hashCode() : 0);
        result = 31 * result + (w3 != null ? w3.hashCode() : 0);
        result = 31 * result + (w4 != null ? w4.hashCode() : 0);
        result = 31 * result + (w5 != null ? w5.hashCode() : 0);
        result = 31 * result + (w6 != null ? w6.hashCode() : 0);
        result = 31 * result + (w7 != null ? w7.hashCode() : 0);
        return result;
    }
}
