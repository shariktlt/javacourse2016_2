package ru.innopolis.k_sharypov.pair_learning_old.entity

import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotEquals;

/**
 * Created by innopolis on 30.10.16.
 */
class UserEntityTest  {
    @Test
    void testGetPassWordHash() {
        for (int i = 0; i < 1000; i++) {
            String s1 = Math.random().toString();
            String s2 = Math.random().toString();
            if(s1.equals(s2)){
                assertEquals(UserEntity.getPassWordHash(s1), UserEntity.getPassWordHash(s2));
            }else{
                assertNotEquals(UserEntity.getPassWordHash(s1), UserEntity.getPassWordHash(s2));
            }

        }

    }
}
